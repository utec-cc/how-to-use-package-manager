/*#include <cassert>
#include <iostream>
#include <string>
#include <vector>
#include <experimental/ranges/algorithm>
#include <experimental/ranges/iterator>

// Qualify everything with "std::experimental::ranges" if you like,
// I prefer to use a namespace alias:
namespace ranges = std::experimental::ranges;

template<class...> class show_type;

template<ranges::Readable T>
void foo(T&) {}

int main() {
    auto v = std::vector<std::string>{"thi2s", "is", "a", "test"};
    ranges::sort(v);
    auto out = ranges::ostream_iterator<std::string>{std::cout, " "};
    ranges::copy(v, out);
    std::cout << '\n';
    auto result = ranges::reverse_copy(v, out);
    std::cout << '\n';
    return !(result.in == ranges::end(v));
}*/

#include <cstdio>
#include<memory>
#include<iostream>
#include <optional>

#include "mpark/patterns.hpp"

using namespace mpark::patterns;
using namespace std;

void fizzbuzz() {

    for (int i = 1; i <= 100; ++i) {
        match(i % 3, i % 5)(
                pattern(0, 0) = [] { std::printf("fizzbuzz\n"); },
                pattern(0, _) = [] { std::printf("fizz\n"); },
                pattern(_, 0) = [] { std::printf("buzz\n"); },
                pattern(_, _) = [i] { std::printf("%d\n", i); });
    }
}

int factorial(int n , int contador = 0 , int acum = 1){
    if (contador == n)
        return acum;
    else
        return factorial(n,contador+1,acum*(contador+1));
}

int mfactorial(int n , int contador = 0 , int acum = 1) {
    return match(n)(
            pattern(0) = [=] {return acum;},
            pattern(_) = [=] {return factorial(n,contador+1,acum*(contador+1)); }
            );
}

int main() {
   // fizzbuzz();
    //int valor = 3;
    //unique_ptr<int> *a = &valor;
    //std::optional<unique_ptr<int>> opuntero = nullopt ;

    //auto oComplex = make_optional<>(3.0, 4.0);

    cout << mfactorial(4);

    int a,b;
    cin >> a >> b;

    auto puntero = make_unique<double>(a);
    auto puntero2 = make_unique<double>(b);

    cout << "suma " << *puntero.get() + *puntero2.get() << "\n";
    cout << "diferencia " << *puntero.get()  - *puntero2.get() << "\n";
    cout << "multiplicacion " << *puntero.get() * *puntero2.get() << "\n";
}